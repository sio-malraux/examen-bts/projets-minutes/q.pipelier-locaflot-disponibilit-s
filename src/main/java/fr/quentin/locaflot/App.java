package fr.quentin.locaflot;

import fr.quentin.locaflot.controllers.MainPanel;
import fr.quentin.locaflot.models.Database;

import javax.swing.*;

public class App {
    public static void main(String[] args){
        Database database = new Database();
        database.config();

        JFrame window = new JFrame("Main");
        window.setContentPane(new MainPanel().getPanel());
        window.setSize(1000,500);
        window.setLocationRelativeTo(null);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);

        MainPanel mainPanel = new MainPanel();
    }
}
