package fr.quentin.locaflot.controllers;

import fr.quentin.locaflot.models.Rent;
import fr.quentin.locaflot.models.Selector;
import fr.quentin.locaflot.models.TypeBoats;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;

public class MainPanel {
    private JPanel panel;
    private DefaultTableModel model;
    private JComboBox cbType;
    private JComboBox cbTime;
    private JLabel title;
    private JButton submitFilter;
    private JLabel labelType;
    private JLabel labelTime;
    private JLabel labelDuration;
    private JTable tableEmbarcation;
    private JTextField textDate;
    private JLabel labelDate;
    private JTextField textDuration;

    public MainPanel() {
        model = new DefaultTableModel();

        model.setColumnIdentifiers(new Object[]{"Name", "Seatings", "Date", "Begin hour", "End hour", "Price (1/2 Hour)", "Price (1 Hour)", "Price (1/2 Day)", "Price (1 Day)"});

        tableEmbarcation.setModel(model);

        refreshTab("");
        refreshFields();

        submitFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.println(textDate.getText());

                if(textDate.getText().length() == 0 || Pattern.matches("[0-9]{4}-[0-1][1-9]-[0-3][0-9]", textDate.getText())){
                    if(textDuration.getText().length() == 0 ||
                            Pattern.matches("[0-6][0-9]m", textDuration.getText()) ||
                            Pattern.matches("[0-1][0-9]h", textDuration.getText()) ||
                            Pattern.matches("[1-7]d", textDuration.getText()) ){
                        refreshTab(cbType.getSelectedItem().toString());
                    }else{
                        JOptionPane.showMessageDialog(null, "Erreur de durée, merci de ré-essayer.");
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Erreur de date, merci de ré-essayer.");
                }
            }
        });
    }

    public JPanel getPanel() {
        return panel;
    }

    private void refreshTab(String dataType){

        model.setRowCount(0);

        for (Rent boat : Selector.getBoatsByFilter(dataType)) {
            String data[] ={boat.getRented_boat().getType().getName(), String.valueOf(boat.getPersons_numbers()), String.valueOf(boat.getContract().getDate()), String.valueOf(boat.getContract().getBegin_hour()), String.valueOf(boat.getContract().getEnd_hour()),
                    String.valueOf(boat.getRented_boat().getType().getHalf_hourly_price()), String.valueOf(boat.getRented_boat().getType().getHourly_price()), String.valueOf(boat.getRented_boat().getType().getHalf_daily_price()), String.valueOf(boat.getRented_boat().getType().getDaily_price())};

            model.addRow(data);
        }
    }

    private void refreshFields(){
        for (String value:
             Selector.getTypeBoats()) {
            cbType.addItem(value);
        }

        for(int i = 0; i <= 23; i++){
            cbTime.addItem(i + "h");
        }
    }
}