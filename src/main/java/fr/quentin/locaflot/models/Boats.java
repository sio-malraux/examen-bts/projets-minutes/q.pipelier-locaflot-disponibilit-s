package fr.quentin.locaflot.models;

public class Boats {

    private String id;
    private String color;
    private boolean available;
    private TypeBoats type;

    public Boats(){
        this.id = "";
        this.color = "color";
        this.available = false;
        this.type = new TypeBoats();
    }

    public Boats(String id, String color, boolean available, TypeBoats type) {
        this.id = id;
        this.color = color;
        this.available = available;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public TypeBoats getType() {
        return type;
    }

    public void setType(TypeBoats type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Boats{" +
                "id='" + id + '\'' +
                ", color='" + color + '\'' +
                ", available=" + available +
                ", type=" + type +
                '}';
    }
}
