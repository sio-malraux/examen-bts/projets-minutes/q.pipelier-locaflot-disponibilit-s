package fr.quentin.locaflot.models;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;

public class Database {

    private static String host;
    private static String database;
    private static String user;
    private static String password;
    private static String port;
    private static Connection connection;
    private static ResultSet resultQuery;

    private static Connection getConnection(){
        try{
            String url = "jdbc:postgresql://" + host + ":" + port + "/" + database;
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }

    public static ResultSet dbQuery(String req){
        try{
            Statement query = getConnection().createStatement();
            resultQuery = query.executeQuery(req);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return resultQuery;
    }

    public void config(){
        JSONParser parser = new JSONParser();
        try {
            Object obj = parser.parse(new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource("config/database.json").toURI()))));

            JSONObject jsonObject = (JSONObject)obj;

            this.host = (String) jsonObject.get("host");
            this.database = (String) jsonObject.get("name");
            this.user = (String) jsonObject.get("user");
            this.password = (String) jsonObject.get("password");
            this.port = (String) jsonObject.get("port");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}