package fr.quentin.locaflot.models;

public class Rent {

    private RentContract contract;
    private Boats rented_boat;
    private int persons_numbers;

    public Rent(){
        this.contract = new RentContract();
        this.rented_boat = new Boats();
        this.persons_numbers = 0;
    }

    public Rent(RentContract contract, Boats rented_boat, int persons_numbers) {
        this.contract = contract;
        this.rented_boat = rented_boat;
        this.persons_numbers = persons_numbers;
    }

    public RentContract getContract() {
        return contract;
    }

    public void setContract(RentContract contract) {
        this.contract = contract;
    }

    public Boats getRented_boat() {
        return rented_boat;
    }

    public void setRented_boat(Boats rented_boat) {
        this.rented_boat = rented_boat;
    }

    public int getPersons_numbers() {
        return persons_numbers;
    }

    public void setPersons_numbers(int persons_numbers) {
        this.persons_numbers = persons_numbers;
    }

    @Override
    public String toString() {
        return "Rent{" +
                "contract=" + contract +
                ", rented_boat=" + rented_boat +
                ", persons_numbers=" + persons_numbers +
                '}';
    }
}
