package fr.quentin.locaflot.models;

import java.sql.Date;
import java.sql.Time;

public class RentContract {

    private int id;
    private Date date;
    private Time begin_hour;
    private Time end_hour;

    public RentContract(){
        this.id = 0;
        this.date = new Date(2000, 0, 0);
        this.begin_hour = new Time(1,0,0);
        this.end_hour = new Time(1,0,0);
    }

    public RentContract(int id, Date date, Time begin_hour, Time end_hour) {
        this.id = id;
        this.date = date;
        this.begin_hour = begin_hour;
        this.end_hour = end_hour;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getBegin_hour() {
        return begin_hour;
    }

    public void setBegin_hour(Time begin_hour) {
        this.begin_hour = begin_hour;
    }

    public Time getEnd_hour() {
        return end_hour;
    }

    public void setEnd_hour(Time end_hour) {
        this.end_hour = end_hour;
    }

    @Override
    public String toString() {
        return "RentContract{" +
                "id=" + id +
                ", date=" + date +
                ", begin_hour=" + begin_hour +
                ", end_hour=" + end_hour +
                '}';
    }
}
