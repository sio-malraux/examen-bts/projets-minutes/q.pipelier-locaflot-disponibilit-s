package fr.quentin.locaflot.models;

import javax.xml.crypto.Data;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Selector {

    private static ArrayList<Rent> listBoats;
    private static ArrayList<String> listTypeBoats;

    public static ArrayList<Rent> getBoatsByFilter(String dataType){
        listBoats = new ArrayList<Rent>();

        ResultSet resultSet = Database.dbQuery(
                "SELECT type_boat.*, contrat.date, contrat.heure_debut, contrat.heure_fin, contrat.id AS cId, embarcation.id AS eId, embarcation.couleur, embarcation.disponible, louer.nb_personnes FROM \"locaflot_dispo\".\"louer\" " +
                        "INNER JOIN \"locaflot_dispo\".\"embarcation\" AS embarcation ON  louer.id_embarcation = embarcation.id " +
                        "INNER JOIN \"locaflot_dispo\".\"type_embarcation\" AS type_boat ON embarcation.type = type_boat.code " +
                        "INNER JOIN \"locaflot_dispo\".\"contrat_location\" AS contrat ON louer.id_contrat = contrat.id " +
                        "WHERE type_boat.nom ~ '" + dataType + "';");

        try {
            while (resultSet.next()){
                TypeBoats boat = new TypeBoats(resultSet.getString("code"), resultSet.getString("nom"), resultSet.getInt("nb_place"), resultSet.getDouble("prix_demi_heure"),
                        resultSet.getDouble("prix_heure"), resultSet.getDouble("prix_demi_jour"), resultSet.getDouble("prix_jour"));

                Rent rent = new Rent(
                        new RentContract(resultSet.getInt("cId"), resultSet.getDate("date"), resultSet.getTime("heure_debut"), resultSet.getTime("heure_fin")),
                        new Boats(resultSet.getString("cId"), resultSet.getString("couleur"), resultSet.getBoolean("disponible"), boat),
                        resultSet.getInt("nb_personnes")
                );

                listBoats.add(rent);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listBoats;
    }

    public static ArrayList<String> getTypeBoats(){
        listTypeBoats = new ArrayList<String>();

        ResultSet resultSet = Database.dbQuery("SELECT DISTINCT(nom) FROM \"locaflot_dispo\".\"type_embarcation\"");

        try{
            while (resultSet.next()){
                listTypeBoats.add(new String(resultSet.getString("nom")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return listTypeBoats;
    }
}
