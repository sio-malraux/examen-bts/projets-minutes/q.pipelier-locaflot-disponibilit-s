package fr.quentin.locaflot.models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TypeBoats {

    private String code;
    private String name;
    private int seating_numbers;
    private double half_hourly_price;
    private double hourly_price;
    private double half_daily_price;
    private double daily_price;

    public TypeBoats(){
        this.code = "";
        this.name = "";
        this.seating_numbers = 0;
        this.half_hourly_price = 0;
        this.hourly_price = 0;
        this.half_daily_price = 0;
        this.daily_price = 0;
    }

    public TypeBoats(String code, String name, int seating_numbers, double half_hourly_price, double hourly_price, double half_daily_price, double daily_price) {
        this.code = code;
        this.name = name;
        this.seating_numbers = seating_numbers;
        this.half_hourly_price = half_hourly_price;
        this.hourly_price = hourly_price;
        this.half_daily_price = half_daily_price;
        this.daily_price = daily_price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSeating_numbers() {
        return seating_numbers;
    }

    public void setSeating_numbers(int seating_numbers) {
        this.seating_numbers = seating_numbers;
    }

    public double getHalf_hourly_price() {
        return half_hourly_price;
    }

    public void setHalf_hourly_price(double half_hourly_price) {
        this.half_hourly_price = half_hourly_price;
    }

    public double getHourly_price() {
        return hourly_price;
    }

    public void setHourly_price(double hourly_price) {
        this.hourly_price = hourly_price;
    }

    public double getHalf_daily_price() {
        return half_daily_price;
    }

    public void setHalf_daily_price(double half_daily_price) {
        this.half_daily_price = half_daily_price;
    }

    public double getDaily_price() {
        return daily_price;
    }

    public void setDaily_price(double daily_price) {
        this.daily_price = daily_price;
    }

    @Override
    public String toString() {
        return "TypeBoats{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", seating_numbers=" + seating_numbers +
                ", half_hourly_price=" + half_hourly_price +
                ", hourly_price=" + hourly_price +
                ", half_daily_price=" + half_daily_price +
                ", daily_price=" + daily_price +
                '}';
    }
}
